# Boot animation
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Enhanced NFC
$(call inherit-product, vendor/cm/config/nfc_enhanced.mk)

# Inherit device configuration
$(call inherit-product, device/lge/vu2u/vu2u.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := vu2u
PRODUCT_NAME := cm_vu2u
PRODUCT_BRAND := lge
PRODUCT_MODEL := LG-F200L
PRODUCT_MANUFACTURER := LGE

PRODUCT_BUILD_PROP_OVERRIDES += 

# Enable Torch
PRODUCT_PACKAGES += Torch

#Nightly Build
CM_BUILDTYPE := NIGHTLY
